# projet-sql

## Contexte
L'objectif du projet sera de faire en groupe la conception du modèle de donnée, la mise en place de la base de données et les composants d'accès aux données pour une application de prise de rendez-vous (type doctolib)

Pas la peine de gérer la partie inscription/compte utilisateur·ice

## Étapes du projet
1. Identifier et lister sous forme de user stories ou de diagramme de use case les fonctionnalités de l'application
2. (Optionnel) Créer les maquettes fonctionnelles de quelques écrans de l'application
3. Identifier et schématiser sous forme de diagramme de classe les entités qui persisteront en base de données pour que ces fonctionnalités soient possibles
4. Créer un ou des scripts SQL qui permettront de mettre en place la base de données, ils contiendront la création des tables et l'insertion d'un jeu de données
5. Dans un projet Java, créer les classes représentant les entités et les composant d'accès aux données en utilisant JDBC
6. (Optionnel) Si vous avez le temps, faire un ou deux écrans avec Spring et Thymeleaf

## Organisation conseillée
* La phase de conception, d'identification des fonctionnalités et des classes est bien à faire de manière collégiale, sur un pc ou sur papier en faisant en sorte que tout le monde puisse s'exprimer
* Créer dès le début un projet Gitlab afin de pouvoir utiliser les outils de ce dernier : les issues pour lister les fonctionnalités et tâche à réaliser
* Réaliser les diagrammes soit sur papier, soit sur staruml2, ou bien sur draw.io ou lucidchart (mais ces deux derniers sont un peu moins agréables à utiliser)
* Lorsque vous commencez à travailler sur des tâches différentes, commencer chaque après midi par un standing meeting afin que chaque membre du groupe sache ses propres tâches et celles des autres
* Créer des scripts SQL séparés pour chaque table+insertion de données afin de plus facilement vous répartir le travail

## Les groupes
* Oualid, Phonevilay, Aïcha
* Khaled, Roland, Nolwenn
* Flo, Magatte, Rafaël
* Yousra, Melwic, Mélissa
* Kevin, Claire, Florian
